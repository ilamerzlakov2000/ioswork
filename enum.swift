enum Color: String {
    case black = "Black"
    case white = "White"
}
enum IndexColor: Int {
    case Black = 0
    case white = 1
}


enum Gender {
    case male
    case female
} 
enum Age {
    case young
    case adult
    case old
}
enum WorkExperience {
    case lessThanOneYear
    case oneThreeYears
    case threeSixYears
}

enum Rainbow {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}



enum toys {
    case car(car: String)
    case doll(doll:String) 
    case lodge(lodge:String) 
    case sword(sword:String) 
}
func outToys() {
    let toys:[toys] = [.car(car:"gas"), .doll(doll:"barbi"), .lodge(lodge:"big lodge"), .sword(sword:"katana")]
    for toy in toys {
        switch toy {
        case .car:
            print("\(toy)")
        case .doll: 
         print("\(toy)") 
         case .lodge: 
         print("\(toy)") 
         case .sword: 
         print("\(toy)")   
        }
    } 
}
outToys()
print("___________________")

enum Score: String {
    case excellent = "excellent"
    case good = "good"
    case satisfactory = "satisfactory"
    case unsatisfactory = "unsatisfactory"
}
func studentAssessment(score:Score) -> Int {
    switch score {
    case .excellent:
    return 5
    case .good:
    return 4
    case .satisfactory:
    return 3
    case .unsatisfactory:
    return 2
    }
}

let student = Score.excellent
let numberScore = studentAssessment(score:student)
print("assessment student is \(student): \(numberScore)")
print("___________________")

enum Garage: String {
    case toyota
    case suzuki 
    case mitsubishi
    case nissan
}
func  CarsInGarage() {
    let garage:[Garage] = [.toyota, .suzuki, .mitsubishi, .nissan]
    for car in garage {
    switch car {
        case .toyota:
            print("\(car) in garage")
        case .suzuki:
            print("\(car) in garage")
        case .mitsubishi:
            print("\(car) in garage")
        case .nissan:
            print("\(car) in garage")
        }
    }
}
CarsInGarage()