
enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(volume: Double)
    case unloadCargo(volume: Double)
}

struct Car {
    let brand: String
    let year: Int
    var cargoVolume: Double
    var engineStarted: Bool
    var windowsOpened: Bool
    
    mutating func performAction(_ action: CarAction) {
        switch action {
        case .startEngine:
            engineStarted = true
        case .stopEngine:
            engineStarted = false
        case .openWindows:
            windowsOpened = true
        case .closeWindows:
            windowsOpened = false
        case .loadCargo(let volume):
            cargoVolume += volume
        case .unloadCargo(let volume):
            cargoVolume -= volume
            if cargoVolume < 0 {
                cargoVolume = 0
            }
        }
    }
}

struct Truck {
    let brand: String
    let year: Int
    var cargoCapacity: Double
    var cargoVolume: Double
    var engineStarted: Bool
    
    mutating func performAction(_ action: CarAction) {
        switch action {
        case .startEngine:
            engineStarted = true
        case .stopEngine:
            engineStarted = false
        case .loadCargo(let volume):
            let freeVolume = cargoCapacity - cargoVolume
            if freeVolume >= volume {
                cargoVolume += volume
            } else {
                print("Not enough space in the truck!")
            }
        case .unloadCargo(let volume):
            cargoVolume -= volume
            if cargoVolume < 0 {
                cargoVolume = 0
            }
        case .openWindows, .closeWindows:
            print("Truck windows cannot be opened or closed.")
        }
    }
}

var car1 = Car(brand: "Toyota", year: 2015, cargoVolume: 300, engineStarted: false, windowsOpened: false)
var car2 = Car(brand: "BMW", year: 2020, cargoVolume: 200, engineStarted: false, windowsOpened: true)
var truck = Truck(brand: "Mercedes-Benz", year: 2010, cargoCapacity: 5000, cargoVolume: 2000, engineStarted: false)

car1.performAction(.startEngine)
car1.performAction(.openWindows)
car1.performAction(.loadCargo(volume: 100))
print("Car 1 cargo volume: \(car1.cargoVolume)")

car2.performAction(.loadCargo(volume: 300))
car2.performAction(.unloadCargo(volume: 100))
print("Car 2 cargo volume: \(car2.cargoVolume)")

truck.performAction(.startEngine)
truck.performAction(.loadCargo(volume: 5000))
truck.performAction(.unloadCargo(volume: 3000))
print("Truck cargo volume: \(truck.cargoVolume)")

var vehiclesDict = ["car1": "car1", "car2": "car2", "truck": "truck"]
print(vehiclesDict.values) // ["car1", "car2", "truck"]

print("_____________________________________________________")

class Man {
    var myCar: CarCar?
    deinit{
        print("мужчина удален из памяти")
    }
}

class CarCar {
    var driver: Man?
    deinit{
        print("машина удалена из памяти")
    }
}

var car: CarCar? = CarCar()
var man: Man? = Man()
car?.driver = man 
man?.myCar = car
car = nil
man = nil
//_________________________________________________

class Creature {
    var passport: Passport?
    deinit {
        print("Существо удалено из памяти")
    }
}

class Passport {
    weak var creature: Creature?
    
    init(creature: Creature) {
        self.creature = creature
    }
    
    deinit {
        print("Паспорт удален из памяти")
    }
}

var creature: Creature? = Creature()
var passport: Passport? = Passport(creature: creature!)
creature?.passport = passport
passport = nil
creature = nil
