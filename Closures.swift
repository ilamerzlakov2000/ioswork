let array = [23, 154, 21, 51, 53, 7, 3, 89, 19, 1000, 1]

func sortArray(num1: Int, num2: Int) -> Bool {
    return num1 < num2
}
func sortArrayRevers(num1: Int, num2: Int) -> Bool {
    return num1 > num2
}

var sortedArray = array.sorted(by: sortArray)
print("Array_1: \(sortedArray)")

var sortedArray2 = array.sorted(by: sortArrayRevers)
print("Array_2: \(sortedArray2)")




func newFriends(names: String...) -> [String] {
    var friends:[String]=[]
    for name in names {
        friends.append(name)
    }
    return friends
}
print("__________________________________________________________________")

let myNewFriend = newFriends(names:"Elisaveta", "Dmitriy", "i1a", "Ivan", "Andrey")
print("friend array 1: \(myNewFriend)")

func sortFriend(name1: String, name2: String) -> Bool {
    return name1.count < name2.count
}

let sortedFriends = myNewFriend.sorted(by: sortFriend)
print("friend array 2: \(sortedFriends)")
print("__________________________________________________________________")

var friends = [Int:String]()
friends[0] = "Ila"
friends[1] = "Gosha"
friends[2] = "Vadim"
friends[3] = "Lisa"
print(friends)

func keyValue(key: Int) {
    if let value = friends[key] {
        print("key: \(key) Value: \(value)")
    }else {
        print("key \(key) is not found")
    }
}
keyValue(key: 3)
keyValue(key: 7)
print("__________________________________________________________________")


func Arrays(ArrInt: inout[Int], ArrString:inout[String]) {
    if  ArrInt.count == 0 {
        ArrInt.append(1)
        print("Int array: \(ArrInt)")
    }
    else {
        print("Int array: \(ArrInt)")
    }

    if  ArrString.count == 0 {
        ArrString.append("Value")
        print("String array: \(ArrString)")
    }
    else {
        print("String array: \(ArrString)")
    }
}
var intArr = [Int]()
// intArr = [1,5]
var stringArr = [String]()
// stringArr = ["tip", "top"]
Arrays(ArrInt:&intArr, ArrString: &stringArr)

