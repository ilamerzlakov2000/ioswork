class Parent {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}

class Child_1: Parent {
    var gender: String 
    init(name: String, age: Int, gender: String) {
        self.gender = gender
        super.init(name:name, age:age)
    }
}

class Child_2 : Parent {
    var hobby: String
    init(name: String, age: Int, hobby: String) {
        self.hobby = hobby
        super.init(name:name, age:age)
    }
}
//______________________


class House {
    var width: Int
    var height: Int

    init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }

    func create() {
        let area  = width * height
        print("areaf = \(area)")
    }
    func destroy() {
            print("House destroyed")    
    }
}
//______________________

class Student {
    let name: String
    let grade: Int
     init(name: String, grade: Int) {
        self.name = name
        self.grade = grade
    }  
   static func sortName(students:[Student]) -> [Student] {
        return students.sorted(by:{$0.name < $1.name})
    }
    static func sortGrade(students:[Student]) -> [Student] {
        return students.sorted(by:{$0.grade < $1.grade})
    }
}

// Example
var students = [
    Student(name: "Alex", grade: 4),
    Student(name: "John", grade: 5),
    Student(name: "Anna", grade: 3)
]

let sortedName = Student.sortName(students:students)
print("sorted by name: \(sortedName)")
let sertedGrade = Student.sortGrade(students:students)
print("sorted by Grade: \(sertedGrade)")
print("________________________________________________________")


struct StructCar {
    var model: String
    var year: Int
    var color: String
}
let car1 = StructCar(model: "Toyota",year: 2000, color:"black" )
print(car1)

class ClassCar {
    var model: String
    var year: Int
    var color: String
    init(model: String, year: Int, color: String) {
        self.model = model
        self.year = year
        self.color = color
    }
}
let car2 = ClassCar(model: "Toyota",year: 2000, color:"black" )
print(car2)
/* Отличаются структуры от классов:
- классы наследуются от других классов, а структуры нет
- при создании класса, все его свойства должны быть инициализированы сразу, а при создании структуры это не обязательно.
*/

let cards = ["2♣️", "3♣️", "4♣️", "5♣️", "6♣️", "7♣️", "8♣️", "9♣️", "10♣️", "J♣️", "Q♣️", "K♣️", "A♣️",
             "2♦️", "3♦️", "4♦️", "5♦️", "6♦️", "7♦️", "8♦️", "9♦️", "10♦️", "J♦️", "Q♦️", "K♦️", "A♦️",
             "2❤️", "3❤️", "4❤️", "5❤️", "6❤️", "7❤️", "8❤️", "9❤️", "10❤️", "J❤️", "Q❤️", "K❤️", "A❤️",
             "2♠️", "3♠️", "4♠️", "5♠️", "6♠️", "7♠️", "8♠️", "9♠️", "10♠️", "J♠️", "Q♠️", "K♠️", "A♠️"]

func chooseFiveCards() -> [String] {
    var cardsInHand: [String] = []
    while cardsInHand.count < 5 {
        let randomNumber = Int.random(in: 0 ..< cards.count)
        let randomCard = cards[randomNumber]
        if !cardsInHand.contains(randomCard) {
            cardsInHand.append(randomCard)
        }
    }
    return cardsInHand
}


let hand = chooseFiveCards()
print(hand)
