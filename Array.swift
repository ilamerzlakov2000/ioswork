let countDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
print("Count days = \(countDays)")

let nameMonth = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
print("Name months = \(nameMonth)")
print("")


print("days")
for  i in 0 ..< countDays.count {
    print("\(i+1): \(countDays[i])")
}
print("_______________")

print("month + days")
var arrayNamesCountDays = nameMonth
for  i in 0 ..< arrayNamesCountDays.count {
    print("\(arrayNamesCountDays[i]): \(countDays[i])")
}
print("_______________")


print("tuples")
for (month, days) in zip(arrayNamesCountDays, countDays) {
    print("(\(month), \(days))")
}
print("_______________")


print("reversed araay")
for  i in (0 ..< arrayNamesCountDays.count).reversed() {
    print("\(arrayNamesCountDays[i]): \(countDays[i])")
}
print()


let dayMonth = (month: 11, day: 16)


var sumDays = 0
for i in 0..<(dayMonth.month-1) {
    sumDays += countDays[i]
}

sumDays += dayMonth.day
print("selected values: \(dayMonth)")

print("days result: \(sumDays)")
